<?php
session_start();
if (isset($_SESSION['username'])) {
    // User is already logged in, show email and logout button
    echo "Hello, " . $_SESSION['username'] . "<br>";
    echo "<form action=\"server.php\" method=\"post\">";
    echo "<input type=\"hidden\" name=\"action\" value=\"logout\"><br>";
    echo "<button type=\"submit\">Logout</button>";
    echo "</form>";
} else {
    // User is not logged in, show login form
    echo "<form action=\"server.php\" method=\"post\">";
    echo "<label>Email:</label><br>";
    echo "<input type=\"text\" name=\"username\"><br>";
    echo "<label>Password:</label><br>";
    echo "<input type=\"password\" name=\"password\"><br>";
    echo "<button type=\"submit\">Login</button>";
    echo "</form>";
}
?>