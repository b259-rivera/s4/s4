<?php
session_start();
if (isset($_POST['action']) && $_POST['action'] == 'logout') {
    // User clicked logout, clear session variables
    session_unset();
    session_destroy();
    header('Location: index.php');
    exit();
}

// Check if login form was submitted
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    // Check if credentials are correct
    if ($username == 'johnsmith@gmail.com' && $password == '1234') {
        // Login successful, set session variables
        $_SESSION['username'] = $username;
        // Redirect to index.php to show logged in status
        header('Location: index.php');
        exit();
    } else {
        // Login failed, show error message
        echo "Invalid username or password";
    }
}
?>